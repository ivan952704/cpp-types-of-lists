// Lista Simple Ptr Final.cpp : main project file.

#include "stdafx.h"
#include "Lista.h"
#include <stdio.h>
#include <conio.h>

using namespace System;

int main(array<System::String ^> ^args)
{
	CLista *objLista = new CLista();

	objLista->InsertarInicio(10);
	objLista->InsertarInicio(-1);
	objLista->InsertarInicio(3);
	objLista->InsertarPosicion(30, 1);
	objLista->InsertarFinal(-8);
	objLista->InsertarFinal(5);
	objLista->InsertarFinal(20);
	objLista->InsertarPosicion(-35, 5);

	objLista->ImprimirLista();

	objLista->EliminarPosicion(4);
	objLista->EliminarFinal();
	objLista->EliminarInicio();
	objLista->EliminarFinal();
	objLista->EliminarPosicion(2);

	objLista->ImprimirLista();

	if (objLista->BuscarDato(1))
		printf(" El 1 si esta");
	else
		printf(" El 1 no esta");

	_getch();
	delete objLista;
    return 0;
}
