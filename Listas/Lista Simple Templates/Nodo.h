
template <class Tipo>
class CNodo
{
private:
	Tipo Dato;
	CNodo<Tipo> *Next;

public:
	CNodo();
	~CNodo();

	Tipo GetDato();
	void SetDato(Tipo Dato);
	CNodo<Tipo> *GetNext();
	void SetNext(CNodo<Tipo> *Next);
};
