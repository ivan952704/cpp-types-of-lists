
#include "stdafx.h"
#include "Lista.h"

template <class Tipo>
CLista<Tipo>::CLista()
{
	Nele = 0;
	First = NULL;
}

template <class Tipo>
CLista<Tipo>::~CLista()
{
	EliminarTodo();
}

template <class Tipo>
void CLista<Tipo>::ImprimirLista(void(*Funcion)(Tipo))
{
	CNodo<Tipo> *objNodo = First;
	while (objNodo != NULL)
	{
		Funcion(objNodo->GetDato());
		printf("Direccion: %p Siguiente: %p", objNodo, objNodo->GetNext());
		 
		objNodo = objNodo->GetNext();
	}
	printf("\n\n");
}

template <class Tipo>
bool CLista<Tipo>::BuscarDato(Tipo Dato)
{
	CNodo<Tipo> *objNodo = First;
	while (objNodo != NULL)
	{
		if (objNodo->GetDato() == Dato)
			return true;
		objNodo = objNodo->GetNext();
	}
	return false;
}

template <class Tipo>
void CLista<Tipo>::InsertarInicio(Tipo Dato)
{
	CNodo<Tipo> *objNodo = new CNodo<Tipo>();
	objNodo->SetDato(Dato);
	objNodo->SetNext(First);

	First = objNodo;
	Nele++;
}

template <class Tipo>
bool CLista<Tipo>::EliminarInicio()
{
	if (Nele == 0)
		return false;

	CNodo<Tipo> *objNodo = First;
	First = objNodo->GetNext();

	delete objNodo;
	objNodo = NULL;

	Nele--;
	return true;
}

template <class Tipo>
void CLista<Tipo>::EliminarTodo()
{
	while (EliminarInicio());
}
