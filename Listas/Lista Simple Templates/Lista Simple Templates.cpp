// Lista Simple Templates.cpp : main project file.

#include "stdafx.h"
#include "Lista.cpp"
#include <iostream>
#include <conio.h>

using namespace std;

void ImprimeEntero(int Dato)
{
	printf("\n Dato: %3d ", Dato);
}

void ImprimeChar(char Dato)
{
	printf("\n Dato: %c ", Dato);
}

void ImprimeDouble(double Dato)
{
	printf("\n Dato: %.2lf ", Dato);
}

void ImprimeString(string Dato)
{
	printf("\n String: %s ", Dato.c_str());
}

void ListarEnteros()
{
	CLista<int> *objLista = new CLista<int>();

	objLista->InsertarInicio(30);
	objLista->InsertarInicio(-1);
	objLista->InsertarInicio(267);

	objLista->ImprimirLista(ImprimeEntero);

	if (objLista->BuscarDato(3))
		printf(" El 3 si esta\n");
	else
		printf(" El 3 no esta\n");

	delete objLista;
}

void ListarChars()
{
	CLista<char> *objLista = new CLista<char>();

	objLista->InsertarInicio('K');
	objLista->InsertarInicio('A');
	objLista->InsertarInicio('R');

	objLista->ImprimirLista(ImprimeChar);

	if (objLista->BuscarDato('A'))
		printf(" La A si esta\n");
	else
		printf(" La A no esta\n");

	delete objLista;
}

void ListarDoubles()
{
	CLista<double> *objLista = new CLista<double>();

	objLista->InsertarInicio(50.7);
	objLista->InsertarInicio(100.42);
	objLista->InsertarInicio(18.9);

	objLista->ImprimirLista(ImprimeDouble);

	if (objLista->BuscarDato(18.8))
		printf(" El 18.8 si esta\n");
	else
		printf(" El 18.8 no esta\n");

	delete objLista;
}

void ListarStrings()
{
	CLista<string> *objLista = new CLista<string>();

	objLista->InsertarInicio("Ivan Contreras");
	objLista->InsertarInicio("Juan Perez");
	objLista->InsertarInicio("Carl Jhonson");

	objLista->ImprimirLista(ImprimeString);

	if (objLista->BuscarDato("Ivan Contreras"))
		printf(" Ivan Contreras si esta\n");
	else
		printf(" Ivan Contreras no esta\n");

	delete objLista;
}

int main(array<System::String ^> ^args)
{
	ListarEnteros();
	ListarChars();
	ListarDoubles();
	ListarStrings();

	_getch();
    return 0;
}
