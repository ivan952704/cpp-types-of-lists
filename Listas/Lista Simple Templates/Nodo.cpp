
#include "stdafx.h"
#include "Nodo.h"
#include <stdio.h>

template <class Tipo>
CNodo<Tipo>::CNodo()
{
	Next = NULL;
}

template <class Tipo>
CNodo<Tipo>::~CNodo()
{
}

template <class Tipo>
Tipo CNodo<Tipo>::GetDato()
{
	return Dato;
}

template <class Tipo>
void CNodo<Tipo>::SetDato(Tipo Dato)
{
	this->Dato = Dato;
}

template <class Tipo>
CNodo<Tipo> *CNodo<Tipo>::GetNext()
{
	return Next;
}

template <class Tipo>
void CNodo<Tipo>::SetNext(CNodo<Tipo> *Next)
{
	this->Next = Next;
}
