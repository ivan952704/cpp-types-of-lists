
#include "Nodo.cpp"

template <class Tipo>
class CLista
{
private:
	int Nele;
	CNodo<Tipo> *First;

public:
	CLista();
	~CLista();

	void ImprimirLista(void(*Funcion)(Tipo));
	bool BuscarDato(Tipo Dato);

	void InsertarInicio(Tipo Dato);

	bool EliminarInicio();
	void EliminarTodo();
};
