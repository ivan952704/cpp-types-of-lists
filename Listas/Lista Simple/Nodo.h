
class CNodo
{
private:
	int Dato;
	CNodo *Next;

public:
	CNodo();
	~CNodo();

	int GetDato();
	void SetDato(int Dato);
	CNodo *GetNext();
	void SetNext(CNodo *Next);
};
