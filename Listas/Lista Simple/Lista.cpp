
#include "stdafx.h"
#include "Lista.h"
#include <stdio.h>

CLista::CLista()
{
	Nele = 0;
	First = NULL;
}

CLista::~CLista()
{
	EliminarTodo();
}

void CLista::ImprimirLista()
{
	CNodo *objNodo = First;
	while (objNodo != NULL)
	{
		printf("\n Dato: %3d Direccion: %p Siguiente: %p", objNodo->GetDato(), objNodo, objNodo->GetNext());
		objNodo = objNodo->GetNext();
	}
	printf("\n\n");
}

bool CLista::BuscarDato(int Dato)
{
	CNodo *objNodo = First;
	while (objNodo != NULL)
	{
		if (objNodo->GetDato() == Dato)
			return true;
		objNodo = objNodo->GetNext();
	}
	return false;
}

void CLista::InsertarInicio(int Dato)
{
	CNodo *objNodo = new CNodo();
	objNodo->SetDato(Dato);
	objNodo->SetNext(First);

	First = objNodo;
	Nele++;
}

void CLista::InsertarFinal(int Dato)
{
	if (Nele == 0)
		return InsertarInicio(Dato);

	CNodo *objAux = First;
	while (objAux->GetNext() != NULL)
		objAux = objAux->GetNext();

	CNodo *objNodo = new CNodo();
	objNodo->SetDato(Dato);

	objAux->SetNext(objNodo);
	Nele++;
}

bool CLista::InsertarPosicion(int Dato, int Pos)
{
	if (Pos < 0 || Pos > Nele)
		return false;

	if (Pos == 0)
	{
		InsertarInicio(Dato);
		return true;
	}

	CNodo *objAux = First;
	int indice = 1;

	while (objAux != NULL)
	{
		if (indice == Pos)
		{
			CNodo *objNodo = new CNodo();
			objNodo->SetDato(Dato);

			objNodo->SetNext(objAux->GetNext());
			objAux->SetNext(objNodo);

			Nele++;
			return true;
		}
		objAux = objAux->GetNext();
		indice++;
	}
}

bool CLista::EliminarInicio()
{
	if (Nele == 0)
		return false;

	CNodo *objNodo = First;
	First = objNodo->GetNext();

	delete objNodo;
	objNodo = NULL;

	Nele--;
	return true;
}

bool CLista::EliminarFinal()
{
	if (Nele <= 1)
		return EliminarInicio();

	CNodo *objAux1 = First->GetNext();
	CNodo *objAux2 = First;

	while (objAux1->GetNext() != NULL)
	{
		objAux2 = objAux1;
		objAux1 = objAux1->GetNext();
	}

	objAux2->SetNext(NULL);
	delete objAux1;
	objAux1 = NULL;
		
	Nele--;
	return true;
}

bool CLista::EliminarPosicion(int Pos)
{
	if (Pos < 0 || Pos > Nele)
		return false;

	if (Pos == 0)
		return EliminarInicio();

	CNodo *objAux1 = First->GetNext();
	CNodo *objAux2 = First;
	int indice = 1;

	while (objAux1 != NULL)
	{
		if (Pos == indice)
		{
			objAux2->SetNext(objAux1->GetNext());
			delete objAux1;
			objAux1 = NULL;

			Nele--;
			return true;
		}
		objAux2 = objAux1;
		objAux1 = objAux1->GetNext();
		indice++;
	}
}

void CLista::EliminarTodo()
{
	while (EliminarInicio());
}
