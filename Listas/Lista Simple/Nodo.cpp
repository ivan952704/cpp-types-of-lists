
#include "stdafx.h"
#include "Nodo.h"
#include <stdio.h>

CNodo::CNodo()
{
	Next = NULL;
}

CNodo::~CNodo()
{
}

int CNodo::GetDato()
{
	return Dato;
}

void CNodo::SetDato(int Dato)
{
	this->Dato = Dato;
}

CNodo *CNodo::GetNext()
{
	return Next;
}

void CNodo::SetNext(CNodo *Next)
{
	this->Next = Next;
}
