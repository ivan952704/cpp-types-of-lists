
#include "Nodo.h"

class CLista
{
private:
	int Nele;
	CNodo *First;

public:
	CLista();
	~CLista();

	void ImprimirLista();
	bool BuscarDato(int Dato);

	void InsertarInicio(int Dato);
	void InsertarFinal(int Dato);
	bool InsertarPosicion(int Dato, int Pos);

	bool EliminarInicio();
	bool EliminarFinal();
	bool EliminarPosicion(int Pos);
	void EliminarTodo();
};
